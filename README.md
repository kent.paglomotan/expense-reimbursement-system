# Expense Reimbursement System
## Description
This project allows employees to create reimbursement requests, view their pending and past reimbursements. On the managers' side, they can view all of the employees' requests and be able to accept or deny them. The managers can also access statistics about the employees such as who has the most expenditure, most approved reimbursements, most denied reimbursements and mean expenditure per employee.
## Technologies Used
* Python - version 3.8
* Flask
* Amazon RDS
* Selenium
* Gherkin
* Javascript
* HTML
* CSS
* Bootstrap
# Features
Functional Features
* Employees can login and view their past reimbursements.
* Employees can view their pending reimbursements.
* Employees can create their own reimbursement with the amount and reason.
* Managers can view all the employees past reimbursements.
* Managers can view all the employees pending reimbursements.
* Managers can approve or deny an emmployee's pending reimbursement based on the ticket ID.
* Managers can view the statistics based on the employees.

To-do list:
* Improving user experience by removing the selection of being an employee and manager in the login page.
* A user's information should persist throughout the web application and ensure their information is secure.
* Add validation for entering the minimum and maximum amount in reimbursement.

# Getting Started
* Create a folder
* Inside the folder ```git init``` to initialize git repository 
* Then use the command below to have the copy the project
```
git clone (url of the project)
git pull origin main
```
# Usage
1. Open the project in Pycharm with the version 3.8.5

2. As soon as everyting is loaded, run main.py
![](./images/main-run.jpg)

3. Once it's running, click the localhost
![](./images/link.jpg)

4. In the login page, there are two right credentials one for the employee and one for the manager.
![](./images/login-page.jpg)

5. If the user is logging in as an employee, the user will be redirected to the employee's home page.
![](./images/emp-home.jpg)

6. The employee can check their past reimbursements
![](./images/emp-past-reimbursement.jpg)

7. The employee can check their pending reimbursement requests
![](./images/emp-pending-request.jpg)

8. The employee can submit a reimbursement
![](./images/submit-reimbursement.jpg)

9. If the user is logging in as a manager, the user will redirected to the manager's dashboard. The manager can view all of the employees reimbursements requests
![](./images/manager-home.jpg)

10. The manager can accept or deny an employee's pending request
![](./images/approve-deny-pending.jpg)

11. The manager can view the statistics of all the employees.
![](./images/statistics.jpg)
