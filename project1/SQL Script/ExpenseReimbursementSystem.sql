
drop table Employee;
create table Employee(
	-- Define our columns
	employee_id int,
	-- To show something to update in the employee table
	employee_firstname varchar(20),
	employee_lastname varchar(20),
	employee_email varchar(30),
	-- use for login
	employee_username varchar(25),
	employee_password varchar(20),
	is_manager boolean,
	-- Primay key would be employeee id
	primary key (employee_id)
);

drop table Reimbursement;
create table Reimbursement(
	ticket_id bigserial,
	employee_id int,
	reimbursement_type varchar(20),
	reimbursement_status varchar(10) DEFAULT 'pending',
	reimbursement_amount money,
	reimbursement_date timestamp default now(),
	reimbursement_purpose varchar(50),
	foreign key (employee_id) references Employee(employee_id),
	primary key (ticket_id)
);

-- Reset table
TRUNCATE TABLE Employee CASCADE;
truncate table Reimbursement;

-- insert data samples Employees and Managers
insert into Employee values(101, 'Bob', 'Wesley', 'bw@revature.net', 'emp101', 'pass101', FALSE);
insert into Employee values(102, 'John', 'Cornell', 'johnc@revature.net', 'emp103', 'pass103', FALSE);
insert into Employee values(103, 'Stanley', 'Cupp', 'stancupp@revature.net', 'emp104', 'pass104', FALSE);
insert into Employee values(104, 'Jim', 'Miffilin', 'jimp@revature.net', 'emp106', 'pass106', FALSE);
insert into Employee values(105, 'Hannah', 'Harper', 'hanH@revature.net', 'emp107', 'pass107', FALSE);
insert into Employee values(1, 'Jeff', 'Nice', 'jn@revature.net', 'emp102', 'pass102', TRUE);
insert into Employee values(2, 'Micheal', 'Scott', 'dunder@revature.net', 'manager', 'password', TRUE);

insert into Reimbursement values (default, 101, 'Certificate', default, 100, default, 'Completed AWS Certificate');
insert into Reimbursement values (default, 101, 'Medical Expense', default, 2500, default, 'Hip Surgery');
insert into Reimbursement values (default, 102, 'Business Expense', default, 5000, default, 'None');
insert into Reimbursement values (default, 102, 'Travel Expense', default, 4200, default, 'Meeting at Arizona HQ');
insert into Reimbursement values (default, 103, 'Certificate', default, 2000, default, 'None');
insert into Reimbursement values (default, 103, 'Travel Expense', default, 1000, default, 'None');
insert into Reimbursement values (default, 104, 'Medical Expense', default, 2200, default, 'Tooth Removal');
insert into Reimbursement values (default, 105, 'Travel Expense', default, 3500, default, 'London Meeting');
insert into Reimbursement values (default, 105, 'Certificate', default, 200, default, 'JAVA and Kubernetes Certified');
insert into Reimbursement values (default, 105, 'Medical Expense', default, 200, default, 'None');


insert into Reimbursement values (default, 101, 'Certificate', default, 300, default, 'OCA Certificate');
insert into Reimbursement values (default, 101, 'Medical Expense', default, 4500, default, 'Dental');

-- View
select * from employee;
select * from reimbursement;
select employee_username, employee_password, is_manager from employee 

where employee_username = 'emp102' and employee_password = 'pass102' and is_manager = TRUE;
select * from Reimbursement where employee_id = 101 order by ticket_id;

-- update from the Manager side to approve and deny
update reimbursement set reimbursement_status = 'approved' 
where employee_id = 101 and reimbursement_type = 'Certificate' and reimbursement_amount::numeric = 100;

update reimbursement set reimbursement_status = 'approved' 
where ticket_id = 20;

update reimbursement set reimbursement_status = 'rejected' 
where ticket_id = 7;

update reimbursement set reimbursement_status = 'approved'
where reimbursement_type = 'Business Expense' and employee_id = 103 and reimbursement_amount::numeric = 5000;

update reimbursement set reimbursement_status = 'rejected' 
where ticket_id = 12; 
--Testing queries
-- Pending query
select * from Reimbursement where reimbursement_status = 'pending' and employee_id = 101;

-- Most Expenditure
select employee_id, sum(reimbursement_amount) as most_expenditure 
from reimbursement group by employee_id order by most_expenditure desc LIMIT 1;

-- Most Reimbursement Approved
select employee_id, count(employee_id) as most_reimbursment_approved
from reimbursement where reimbursement_status = 'approved'
group by employee_id order by most_reimbursment_approved desc LIMIT 1;

-- Most Reimbursement Denied
select employee_id, count(employee_id) as most_reimbursment_denied
from reimbursement where reimbursement_status = 'rejected'
group by employee_id order by most_reimbursment_denied desc LIMIT 1;

-- Mean Expenditure Per Employee
select employee_id, avg(reimbursement_amount::numeric) as mean_expenditure
from reimbursement group by employee_id order by mean_expenditure desc;
