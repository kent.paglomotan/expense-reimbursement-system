from selenium import webdriver
from features.poms.login_page import LoginPage
from features.poms.employee_home_page import EmployeeHomePage
from features.poms.manager_home_page import ManagerHomePage


def before_all(context):
    context.driver = webdriver.Firefox()
    context.in_login_page = LoginPage(context.driver)
    context.employee_home_page = EmployeeHomePage(context.driver)
    context.manager_home_page = ManagerHomePage(context.driver)


def before_step(context, step):
    context.driver.implicitly_wait(5)


def after_all(context):
    # at the end of each test by close the browser window
    context.driver.quit()
