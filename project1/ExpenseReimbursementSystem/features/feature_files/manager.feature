# Functionality about the manager
  Feature: Manager Functionality
    Background:
      Given a user is logged in as manager

    Scenario: A user would like to approve one reimbursement from the pending request
      Given the user is on the pending tab
      When a user enters the ticket id and clicks the approve button
      Then the user is redirected to the home page

    Scenario:  A user would like to deny one reimbursement from the pending request
      Given the user is going to the pending tab
      When a user enters the ticket id and clicks the deny button
      Then the user is back in the home page

    Scenario: A user would like to view the statistics page and requests history
      Given the user is going to the statistics tab and requests history tab
      Then the user is still on the manager's dashboard