# Testing the login functionality
  Feature: Login
    Scenario: A user is on the login page and would like to login with correct credentials
      Given a user is on the login page
      When a user enters the correct username and the correct password and the user pushes the submit button to login
      Then the user is logged in and redirected to the home page
