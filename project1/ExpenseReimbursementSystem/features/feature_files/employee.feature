# Functionality about the employee
  Feature: Employee Functionality
    Background:
      Given a user is logged in

    Scenario: A user would like to submit a new reimbursement request
      When a user enters employee id, amount, type of reimbursement and purpose and the user pushes the submit button
      Then the user checks the request history page

    Scenario: A user would like to view all past reimbursements
      Given a user is on the reimbursement page
      Then the user goes back to the home page

    Scenario: A user would like to view all pending reimbursements
      Given a user is on the pending reimbursements page
      Then the user decides to back to the home page