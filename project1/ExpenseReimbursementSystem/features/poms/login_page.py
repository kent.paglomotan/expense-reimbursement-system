class LoginPage:
    username_id = 'username'
    password_id = 'password'
    employee_id = 'employee'
    manager_id = 'manager'
    login_button_id = 'login-button'

    def __init__(self, driver):
        self.driver = driver

    def enter_employee_credentials(self):
        self.driver.find_element_by_id(self.username_id).send_keys('emp101')
        self.driver.find_element_by_id(self.password_id).send_keys('pass101')
        self.driver.find_element_by_id(self.employee_id).click()

    def enter_manager_credentials(self):
        self.driver.find_element_by_id(self.username_id).send_keys('manager')
        self.driver.find_element_by_id(self.password_id).send_keys('password')
        self.driver.find_element_by_id(self.manager_id).click()

    def click_login_button(self):
        self.driver.find_element_by_id(self.login_button_id).click()

