class EmployeeHomePage:
    username_id = 'username'
    password_id = 'password'
    employee_id = 'employee'
    login_button_id = 'login-button'
    # Submitting
    emp_id_box = 'emp_id_input'
    amount_box = 'amount_input'
    select_type_reimbursement = 'reimbursement_input'
    option_one = 'business'
    purpose_box = 'message_input'
    submit_button = 'submit-request'
    go_to_submit_page_id = 'submit-reimbursement'
    # Viewing past and pending reimbursements
    go_to_request_history_id = 'view-request-history'
    go_to_pending_requests_id = 'view-pending'
    home_button = 'btn'

    def __init__(self, driver):
        self.driver = driver

    def enter_employee_credentials(self):
        self.driver.find_element_by_id(self.username_id).send_keys('emp101')
        self.driver.find_element_by_id(self.password_id).send_keys('pass101')
        self.driver.find_element_by_id(self.employee_id).click()

    def click_login_button(self):
        self.driver.find_element_by_id(self.login_button_id).click()

    def go_to_submit_page(self):
        self.driver.find_element_by_id(self.go_to_submit_page_id).click()

    def go_to_request_history(self):
        self.driver.find_element_by_id(self.go_to_request_history_id).click()

    def go_to_pending_request(self):
        self.driver.find_element_by_id(self.go_to_pending_requests_id).click()

    def create_new_reimbursement(self):
        self.driver.find_element_by_id(self.emp_id_box).send_keys(101)
        self.driver.find_element_by_id(self.amount_box).send_keys(3000)
        self.driver.find_element_by_id(self.select_type_reimbursement).click()
        self.driver.find_element_by_id(self.option_one).click()
        self.driver.find_element_by_id(self.purpose_box).send_keys('Bought a storage unit')

    def click_submit_button(self):
        self.driver.find_element_by_id(self.submit_button).click()

    def click_home_button(self):
        self.driver.find_element_by_class_name(self.home_button).click()
