class ManagerHomePage:
    username_id = 'username'
    password_id = 'password'
    manager_id = 'manager'
    login_button_id = 'login-button'
    req_history_tab_id = 'one-tab'
    pending_tab_id = 'two-tab'
    stat_tab_id = 'three-tab'
    ticket_id_xpath_one = '/html/body/div/div[2]/div[2]/div[2]/table/tr[2]/td[7]'
    this_ticket_id = 0
    ticket_box_id = 'ticket_id_input'
    accept_button = 'approve-reimbursement'
    deny_button = 'deny-reimbursement'

    def __init__(self, driver):
        self.driver = driver

    def enter_manager_credentials(self):
        self.driver.find_element_by_id(self.username_id).send_keys('manager')
        self.driver.find_element_by_id(self.password_id).send_keys('password')
        self.driver.find_element_by_id(self.manager_id).click()

    def click_login_button(self):
        self.driver.find_element_by_id(self.login_button_id).click()

    def get_ticket_id(self):
        self.this_ticket_id = self.driver.find_element_by_xpath(self.ticket_id_xpath_one).text

    def click_pending_tab(self):
        self.driver.find_element_by_id(self.pending_tab_id).click()

    def click_req_history_tab(self):
        self.driver.find_element_by_id(self.req_history_tab_id).click()

    def click_statistics_tab(self):
        self.driver.find_element_by_id(self.stat_tab_id).click()

    def approve_reimbursement(self):
        self.driver.find_element_by_id(self.ticket_box_id).send_keys(self.this_ticket_id)
        self.driver.find_element_by_id(self.accept_button).click()

    def deny_reimbursement(self):
        self.driver.find_element_by_id(self.ticket_box_id).send_keys(self.this_ticket_id)
        self.driver.find_element_by_id(self.deny_button).click()