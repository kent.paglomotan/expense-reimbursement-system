import time

from behave import *


@given('a user is logged in as manager')
def test_manager_logged_in(context):
    context.driver.get('http://127.0.0.1:5000/login')
    assert 'login' in context.driver.current_url
    time.sleep(1)
    context.manager_home_page.enter_manager_credentials()
    context.manager_home_page.click_login_button()
    assert 'home' in context.driver.current_url


@given('the user is on the pending tab')
def test_click_pending_tab(context):
    time.sleep(1)
    context.manager_home_page.click_pending_tab()


@given('the user is going to the statistics tab and requests history tab')
def test_click_statistics_tab_and_request_history(context):
    context.manager_home_page.click_statistics_tab()
    time.sleep(1)
    context.manager_home_page.click_req_history_tab()
    time.sleep(1)


@given('the user is going to the pending tab')
def test_going_to_pending_tab(context):
    time.sleep(1)
    context.manager_home_page.click_pending_tab()


@when('a user enters the ticket id and clicks the approve button')
def test_approve_reimbursement(context):
    context.manager_home_page.get_ticket_id()
    context.manager_home_page.approve_reimbursement()


@when('a user enters the ticket id and clicks the deny button')
def test_deny_reimbursement(context):
    context.manager_home_page.get_ticket_id()
    context.manager_home_page.deny_reimbursement()


@then('the user is redirected to the home page')
def test_is_in_manager_home_page(context):
    time.sleep(1)
    assert 'home' in context.driver.current_url


@then('the user is back in the home page')
def test_is_in_manager_home_page(context):
    time.sleep(1)
    assert 'home' in context.driver.current_url


@then('the user is still on the manager\'s dashboard')
def test_still_in_manger_home_page(context):
    time.sleep(1)
    assert 'home' in context.driver.current_url
