import time

from behave import *


@given('a user is logged in')
def test_logged_in(context):
    context.driver.get('http://127.0.0.1:5000/login')
    assert 'login' in context.driver.current_url
    time.sleep(1)
    context.in_login_page.enter_employee_credentials()
    context.in_login_page.click_login_button()
    assert 'home' in context.driver.current_url


@given('a user is on the reimbursement page')
def test_go_to_reimbursement_page(context):
    context.employee_home_page.go_to_request_history()
    time.sleep(1)
    assert 'reimbursements' in context.driver.current_url


@given('a user is on the pending reimbursements page')
def test_go_to_pending_page(context):
    context.employee_home_page.go_to_pending_request()
    time.sleep(1)
    assert 'pending' in context.driver.current_url


@when('a user enters employee id, amount, type of reimbursement and purpose and the user pushes the submit button')
def test_user_creates_reimbursement_and_submit(context):
    time.sleep(1)
    assert 'home' in context.driver.current_url
    context.employee_home_page.go_to_submit_page()
    assert 'submit' in context.driver.current_url
    context.employee_home_page.create_new_reimbursement()
    context.employee_home_page.click_submit_button()


@then('the user checks the request history page')
def check_requests_history_page(context):
    context.employee_home_page.go_to_request_history()
    assert 'reimbursements' in context.driver.current_url


@then('the user goes back to the home page')
def test_go_back_to_home_page(context):
    context.employee_home_page.click_home_button()
    time.sleep(1)
    assert 'home' in context.driver.current_url


@then('the user decides to back to the home page')
def test_go_home_page(context):
    context.employee_home_page.click_home_button()
    time.sleep(1)
    assert 'home' in context.driver.current_url
