import time

from behave import *


@given('a user is on the login page')
def test_on_login_page(context):
    context.driver.get('http://127.0.0.1:5000/login')
    assert 'login' in context.driver.current_url


@when('a user enters the correct username and the correct password and the user pushes the submit button to login')
def test_user_clicks_submit(context):
    time.sleep(2)
    context.in_login_page.enter_employee_credentials()
    context.in_login_page.click_login_button()


@then('the user is logged in and redirected to the home page')
def test_user_redirected_after_login_success(context):
    assert 'home' in context.driver.current_url
