from src.controller.flaskconfig import app
import src.controller.employee_controller
import src.controller.reimbursement_controller

# This is where we run th app fro maintainability and scalability
if __name__ == '__main__':
    # app.secret_key = 'super_secret'
    app.run()