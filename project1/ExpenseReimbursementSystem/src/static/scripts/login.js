function isEmpty(obj) {
    return Object.keys(obj).length === 0;
}

async function postFormDataAsJson({ url, formData }) {
	const plainFormData = Object.fromEntries(formData.entries());
	const formDataJsonString = JSON.stringify(plainFormData);

	const fetchOptions = {
		method: "POST",
		headers: {
			"Content-Type": "application/json",
			Accept: "application/json",
		},
		body: formDataJsonString,
	};

	const response = await fetch(url, fetchOptions);

	if (!response.ok) {
		const errorMessage = await response.text();
		throw new Error(errorMessage);
	}

	return response.json();
}

/**
 * Event handler for a form submit event.
 *
 * @see https://developer.mozilla.org/en-US/docs/Web/API/HTMLFormElement/submit_event
 *
 * @param {SubmitEvent} event
 */
async function handleFormSubmit(event) {
	event.preventDefault();

	const form = event.currentTarget;
	const url = form.action;


    const formData = new FormData(form);
    const responseData = await postFormDataAsJson({ url, formData });

    if (isEmpty(responseData)) {
        const loginForm = document.getElementById("login");
        setFormMessage(loginForm, "error", "Invalid username or password!");
    }
    else {
        window.location.replace('http://localhost:5000/employee/home')
    }
    console.log({ responseData });

}

const exampleForm = document.getElementById("login-form");
exampleForm.addEventListener("submit", handleFormSubmit);
