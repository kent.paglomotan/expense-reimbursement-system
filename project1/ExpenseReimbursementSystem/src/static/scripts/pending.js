// URLs
let url_employee_pending = 'http://127.0.0.1:5000/employee/pending'


// IDs
let emp_pending_table = '#view-emp-pending-req'
let emp_reimbursements_table = '#view-emp-reimbursements'

// Headers
let reimbursements_header = ['Employee ID', 'Amount', 'Submitted Date', 'Purpose', 'Status', 'Type', 'Ticket ID']


async function get_tables(url, table_id, col_header) {
    let response = await fetch(url)
    let reimbursements = await response.json()
    console.log(reimbursements)
    let myTable = document.querySelector(table_id)
    let headers = col_header

    let table = document.createElement('table')
    let headerRow = document.createElement('tr')

    headers.forEach(headerText => {
        let header = document.createElement('th')
        let textNode = document.createTextNode(headerText)
        header.appendChild(textNode)
        headerRow.appendChild(header)
    });


    table.appendChild(headerRow)

    reimbursements.forEach(rms => {
        let row = document.createElement('tr')

        Object.values(rms).forEach(text => {
            let cell = document.createElement('td')
            let textNode = document.createTextNode(text)
            cell.appendChild(textNode)
            row.appendChild(cell)
        });

        table.appendChild(row)

    });


    myTable.appendChild(table)

}
// Convert this to button later or move it around
window.onload = function () {
    this.get_tables(url_employee_pending, emp_pending_table, reimbursements_header)
//    this.get_tables(url_employee_reimbursements, emp_reimbursements_table, reimbursements_header)
}
