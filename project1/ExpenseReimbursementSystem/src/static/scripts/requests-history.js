
// URLs
let url_reimbursements = 'http://127.0.0.1:5000/employee/requests'
let url_pending = 'http://127.0.0.1:5000/employee/pending-requests'
let url_most_expense = 'http://127.0.0.1:5000/employee/most-expenditure'
let url_most_approved = 'http://127.0.0.1:5000/employee/most-approved'
let url_most_denied = 'http://127.0.0.1:5000/employee/most-denied'
let url_mean_expense = 'http://127.0.0.1:5000/employee/mean-expenditure'
let url_employee_pending = 'http://127.0.0.1:5000/employee/pending'
let url_employee_reimbursements = 'http://127.0.0.1:5000/employee/reimbursements'

// IDs
let first_table = '#request-history-table'
let second_table = '#pending-requests-table'
let most_expense_table = '#most-expenditure'
let most_approved_table = '#approved-reimbursements'
let most_denied_table = '#denied-reimbursements'
let mean_expense_table = '#mean-expenditure'
let emp_pending_table = '#view-emp-pending-req'
let emp_reimbursements_table = '#view-emp-reimbursements'

// Headers
let reimbursements_header = ['Employee ID', 'Amount', 'Submitted Date', 'Purpose', 'Status', 'Type', 'Ticket ID']
let most_expense_header = ['Employee ID', 'Total Expenditure']
let most_approved_header = ['Employee ID', 'Total Approved Reimbursements']
let most_denied_header = ['Employee ID', 'Total Denied Reimbursements']
let mean_expense_header = ['Employee ID', 'Mean Expense Per Employee']


async function get_tables(url, table_id, col_header) {
    let response = await fetch(url)
    let reimbursements = await response.json()
    console.log(reimbursements)
    let myTable = document.querySelector(table_id)
    let headers = col_header

    let table = document.createElement('table')
    let headerRow = document.createElement('tr')

    headers.forEach(headerText => {
        let header = document.createElement('th')
        let textNode = document.createTextNode(headerText)
        header.appendChild(textNode)
        headerRow.appendChild(header)
    });


    table.appendChild(headerRow)

    reimbursements.forEach(rms => {
        let row = document.createElement('tr')

        Object.values(rms).forEach(text => {
            let cell = document.createElement('td')
            let textNode = document.createTextNode(text)
            cell.appendChild(textNode)
            row.appendChild(cell)
        });

        table.appendChild(row)

    });


    myTable.appendChild(table)

}
// Convert this to button later or move it around
window.onload = function () {
    // All reimbursements of Employees - Manager View
    this.get_tables(url_reimbursements, first_table, reimbursements_header)
    this.get_tables(url_pending, second_table, reimbursements_header)
    // Statistics - Manager View
    this.get_tables(url_most_expense, most_expense_table, most_expense_header)
    this.get_tables(url_most_approved, most_approved_table, most_approved_header)
    this.get_tables(url_most_denied, most_denied_table, most_denied_header)
    this.get_tables(url_mean_expense, mean_expense_table, mean_expense_header)
    // Employee View
    this.get_tables(url_employee_pending, emp_pending_table, reimbursements_header)
    this.get_tables(url_employee_reimbursements, emp_reimbursements_table, reimbursements_header)
}
