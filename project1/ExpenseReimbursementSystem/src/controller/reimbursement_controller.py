from src.controller.flaskconfig import app
from flask import request, render_template, redirect, url_for
import src.services.reimbursement_service as r_service
from src.logging.logging_setup import reimbursementLog


# Handles the Server Side where data is being pass through JS
# Reimbursement Requests
@app.route('/employee/requests', methods=['GET'])
def get_reimbursements():
    return r_service.get_reimbursement_requests()


# Pending Requests
@app.route('/employee/pending-requests', methods=['GET'])
def pending_requests_manager():
    return r_service.pending_request()


# Statistics
@app.route('/employee/most-expenditure', methods=['GET'])
def get_most_expenditure():
    return r_service.get_most_expenditure()


@app.route('/employee/most-approved', methods=['GET'])
def get_most_approved():
    return r_service.get_most_approved()


@app.route('/employee/most-denied', methods=['GET'])
def get_most_denied():
    return r_service.get_most_denied()


@app.route('/employee/mean-expenditure', methods=['GET'])
def get_mean_expenditure_per_employee():
    return r_service.get_mean_expenditure_per_employee()


@app.route('/ticket/approve', methods=['POST'])
def accept_request():
    ticket_id = int(request.form.get('ticket_id'))
    reimbursementLog.info('Controller: Accepting this ticket ID: ' + str(ticket_id))
    r_service.accept_requests(ticket_id)
    return redirect(url_for('manager'))


@app.route('/ticket/deny', methods=['POST'])
def deny_request():
    ticket_id = int(request.form.get('ticket_id'))
    reimbursementLog.info('Controller: Denying this ticket ID: ' + str(ticket_id))
    r_service.deny_requests(ticket_id)
    return redirect(url_for('manager'))
