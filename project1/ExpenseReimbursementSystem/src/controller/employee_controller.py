from src.controller.flaskconfig import app
from flask import request, render_template, redirect, session, url_for, make_response
import src.services.employee_service as e_service
import src.services.reimbursement_service as r_service
from src.logging.logging_setup import employeeLog


# Handling the UI side
@app.route('/', methods=['GET'])
def default():
    return redirect(url_for('login_page'))


@app.route('/login', methods=['GET'])
def login_page():
    return render_template('login.html')


@app.route('/submit', methods=['GET'])
def submit():
    return render_template('employee-submit.html')


@app.route('/pending', methods=['GET'])
def pending_view():
    return render_template('employee-pending.html')


@app.route('/reimbursements', methods=['GET'])
def emp_reimbursements_view():
    return render_template('employee-reimbursements.html')


@app.route('/employee/home', methods=['GET'])
def emp_home():
    return render_template('employee-home.html')


@app.route('/manager/home', methods=['GET'])
def manager():
    return render_template('manager-home.html')


@app.route('/invalid', methods =['GET'])
def invalid():
    return render_template('invalid.html')


# Handling the Server side
@app.route('/employee/login', methods=['GET', 'POST'])
def login():
    username = request.form.get('username')
    password = request.form.get('password')
    manager_dict = {'0': False, '1': True}  # To make sure that a boolean is submitted to the db
    is_manager = manager_dict[request.form['user']]

    employeeLog.info('The credentials are Username:' + str(username) + ' Password: ' + str(password)
                     + 'Is it a Manager' + str(is_manager))

    res = e_service.get_user_pass(username, password, is_manager)

    if res != []:
        if is_manager:
            return redirect(url_for('manager'))
        else:
            return redirect(url_for('emp_home'))
    else:
        return redirect(url_for('invalid'))


@app.route('/employee/pending', methods=['GET', 'POST'])
def pending_requests():
    # for testing
    json = r_service.pending_request_by_emp_id(101)
    return json


@app.route('/employee/reimbursements', methods=['GET'])
def emp_reimbursements():
    # for testing
    json = r_service.get_reimbursement_requests_by_emp_id(101)
    return json


@app.route('/employee/submit', methods=['POST'])
def submit_reimbursement():
    emp_id = int(request.form.get('employee_id'))
    amount = int(request.form.get('amount'))
    reimbursement_type = request.form.get('reimbursement_type')
    purpose = request.form.get('message')
    employeeLog.info('The Employee ID:' + str(emp_id) + ' submitted the Type of Reimbursement: '
                     + str(reimbursement_type) + ' the Amount' + str(amount) + ' its Purpose' + str(purpose))
    r_service.submit_request(emp_id, reimbursement_type, amount, purpose)
    return redirect(url_for("emp_home"))

