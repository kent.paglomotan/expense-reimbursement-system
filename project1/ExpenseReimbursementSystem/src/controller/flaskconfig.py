from datetime import timedelta
from flask import Flask
from flask_session import Session
from flask_cors import CORS, cross_origin


app = Flask(__name__, template_folder="../static/templates", static_folder="../static")
app.secret_key = "secret"
app.config["SESSION_PERMANENT"] = False
app.config["SESSION_TYPE"] = "filesystem"
app.config["SESSION_USE_SIGNER"] = True
app.permanent_session_lifetime = timedelta(minutes=5)
# app.config.from_object(__name__)
# Session(app)
# CORS(app)
# app.config.update(SESSION_COOKIE_SAMESITE="None", SESSION_COOKIE_SECURE=True)


@app.after_request
def set_headers(resp):
    resp.headers['Access-Control-Allow-Origin'] = '*'
    return resp