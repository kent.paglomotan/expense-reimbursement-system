from src.models.reimbursement import Reimbursement
from src.models.reimbursement import Statistics
from src.models.reimbursement import MeanExpenditure
from flask import make_response, jsonify
import src.dao.reimbursement_dao as rdao
from src.logging.logging_setup import reimbursementLog


def get_reimbursement_requests():
    # Something to return a dictionary because flask likes dictionary and they are easy to jsonify
    reimbursementLog.info('SERVICE: All of the Reimbursement Requests for all employees')
    db_requests = rdao.get_reimbursement_requests()
    json_data = []
    req_dict = {}
    for values in db_requests:
        requests = Reimbursement(values[0], values[1], values[2], values[3], values[4], values[5], values[6])
        for attr, value in requests.__dict__.items():
            req_dict.update({attr: value})
        json_data.append(req_dict.copy())
    return make_response(jsonify(json_data), 200)


def submit_request(employee_id, reimbursement_type, amount, purpose):
    return rdao.submit_request(employee_id, reimbursement_type, amount, purpose)


def pending_request():
    reimbursementLog.info('SERVICE: All of Pending Requests for all employees')
    db_requests = rdao.pending_request()
    json_data = []
    req_dict = {}
    for values in db_requests:
        requests = Reimbursement(values[0], values[1], values[2], values[3], values[4], values[5], values[6])
        for attr, value in requests.__dict__.items():
            req_dict.update({attr: value})
        json_data.append(req_dict.copy())
    return make_response(jsonify(json_data), 200)


def get_reimbursement_requests_by_emp_id(emp_id):
    reimbursementLog.info('SERVICE: Reimbursements for this Employee ID: ' + str(emp_id))
    db_requests = rdao.get_reimbursement_requests_by_emp_id(emp_id)
    json_data = []
    req_dict = {}
    for values in db_requests:
        requests = Reimbursement(values[0], values[1], values[2], values[3], values[4], values[5], values[6])
        for attr, value in requests.__dict__.items():
            req_dict.update({attr: value})
        json_data.append(req_dict.copy())
    return make_response(jsonify(json_data), 200)


def pending_request_by_emp_id(emp_id):
    reimbursementLog.info('SERVICE: Pending Reimbursements for this Employee ID: ' + str(emp_id))
    db_requests = rdao.pending_request_by_emp_id(emp_id)
    json_data = []
    req_dict = {}
    for values in db_requests:
        requests = Reimbursement(values[0], values[1], values[2], values[3], values[4], values[5], values[6])
        for attr, value in requests.__dict__.items():
            req_dict.update({attr: value})
        json_data.append(req_dict.copy())

    return make_response(jsonify(json_data), 200)


def accept_requests(ticket_id):
    reimbursementLog.info('SERVICE: Accepting this ticket ID: ' + str(ticket_id))
    rdao.accept_request(ticket_id)
    return make_response({"Message": "Tickets were updated"}, 200)


def deny_requests(ticket_id):
    reimbursementLog.info('SERVICE: Denying this ticket ID: ' + str(ticket_id))
    rdao.deny_request(ticket_id)
    return make_response({"Message": "Tickets were updated"}, 200)


def get_most_expenditure():
    reimbursementLog.info('SERVICE: Statistics - Getting the employee who spends the most')
    db_requests = rdao.get_most_expenditure()
    json_data = []
    req_dict = {}
    for values in db_requests:
        requests = Statistics(values[0], values[1])
        for attr, value in requests.__dict__.items():
            req_dict.update({attr: value})
        json_data.append(req_dict.copy())
    return make_response(jsonify(json_data), 200)


def get_most_approved():
    reimbursementLog.info('SERVICE: Statistics - Getting the employee who have most approved reimbursements')
    db_requests = rdao.get_most_approved()
    json_data = []
    req_dict = {}
    for values in db_requests:
        requests = Statistics(values[0], values[1])
        for attr, value in requests.__dict__.items():
            req_dict.update({attr: value})
        json_data.append(req_dict.copy())
    return make_response(jsonify(json_data), 200)


def get_most_denied():
    reimbursementLog.info('SERVICE: Statistics - Getting the employee who have most denied reimbursements')
    db_requests = rdao.get_most_denied()
    json_data = []
    req_dict = {}
    for values in db_requests:
        requests = Statistics(values[0], values[1])
        for attr, value in requests.__dict__.items():
            req_dict.update({attr: value})
        json_data.append(req_dict.copy())
    return make_response(jsonify(json_data), 200)


def get_mean_expenditure_per_employee():
    reimbursementLog.info('SERVICE: Statistics - Getting the mean expenditure per employee')
    db_requests = rdao.get_mean_expenditure_per_employee()
    json_data = []
    req_dict = {}
    for values in db_requests:
        requests = MeanExpenditure(values[0], values[1])
        for attr, value in requests.__dict__.items():
            req_dict.update({attr: value})
        json_data.append(req_dict.copy())
    return make_response(jsonify(json_data), 200)
