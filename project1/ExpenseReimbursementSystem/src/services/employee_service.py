import src.dao.employee_dao as edao
from src.models.employee import Employee, Login
from flask import make_response, jsonify


def get_employee_info():
    # Something to return a dictionary because flask likes dictionary and they are easy to jsonify
    employee_dict = {}
    db_employee = edao.get_employee_info()
    for employee in db_employee:
        employee_dict[employee[0]] = Employee(employee[0], employee[1], employee[2],
                                              employee[3], employee[4], employee[5], employee[6])

    return employee_dict


def get_user_pass(username, password, is_manager):
    db_employee = edao.get_user_pass(username, password, is_manager)
    return db_employee


def get_user_pass_v2(username, password, is_manager):
    db_requests = edao.get_user_pass(username, password, is_manager)
    json_data = []
    req_dict = {}
    for values in db_requests:
        requests = Login(values[0], values[1], values[2])
        for attr, value in requests.__dict__.items():
            req_dict.update({attr: value})
        json_data.append(req_dict.copy())
    return make_response(jsonify(json_data), 200)

