# This module is for connecting to the database
# Importing the connect function to establish a connection from our DB using pyodbc
from pyodbc import connect

# Importing "os" in order to pull environment variables
import os

db_url = os.environ['db_url']
db_username = os.environ['db_username']
db_password = os.environ['db_password']
db_port = os.environ['db_port']
db_name = os.environ['db_name']


def get_connection():
    return connect(
        f"DRIVER={{PostgreSQL Unicode}};SERVER={db_url};PORT=5432;DATABASE={db_name};UID={db_username};PWD={db_password};Trusted_Connection=no")

# Connection successful!
get_connection()