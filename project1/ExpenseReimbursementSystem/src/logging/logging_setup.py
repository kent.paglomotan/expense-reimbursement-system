import logging


def setup_logger(logger_name, log_file, level=logging.DEBUG):
    l = logging.getLogger(logger_name)
    formatter = logging.Formatter('[ %(asctime)s ] [ %(levelname)-5s ] %(message)s')
    fileHandler = logging.FileHandler(log_file, mode='w')
    fileHandler.setFormatter(formatter)
    streamHandler = logging.StreamHandler()
    streamHandler.setFormatter(formatter)

    l.setLevel(level)
    l.addHandler(fileHandler)
    l.addHandler(streamHandler)


# # Initialize the logger for employeeLog
setup_logger('employeeLog', r'..\employee.log')
employeeLog = logging.getLogger('employeeLog')
#
# # Initial set up for reimbursement logging functionality
setup_logger('reimbursementLog', r'..\reimbursement.log')
reimbursementLog = logging.getLogger('reimbursement.log')
