import unittest
from unittest.mock import Mock
from src.services import reimbursement_service as rs
from src.dao import reimbursement_dao as rd
from src.controller.flaskconfig import app


class TestReimbursementService(unittest.TestCase):
    def setUp(self):
        self.app_context = app.app_context()
        self.app_context.push()
        rd.get_reimbursement_requests = Mock(return_value=[(1, 100, 'Certificate', 'pending', 100,
                                                            '2021-07-07 20:49:03', 'None')])
        rd.pending_request = Mock(return_value=[(1, 100, 'Certificate', 'pending', 100,
                                                 '2021-07-07 20:49:03', 'None')])
        rd.get_reimbursement_requests_by_emp_id = Mock(return_value=[(1, 100, 'Certificate', 'pending', 100,
                                                                      '2021-07-07 20:49:03', 'None')])
        rd.pending_request_by_emp_id = Mock(return_value=[(1, 100, 'Certificate', 'pending', 100,
                                                           '2021-07-07 20:49:03', 'None')])
        rd.get_most_expenditure = Mock(return_value=[(100, 4500)])
        rd.get_most_approved = Mock(return_value=[(100, 4)])
        rd.get_most_denied = Mock(return_value=[(101, 2)])
        rd.get_mean_expenditure_per_employee = Mock(return_value=[(100, 2000), (101, 300), (102, 4000)])

    def tearDown(self):
        self.app_context.pop()

    def test_get_reimbursement_requests(self):
        self.assertEqual(rs.get_reimbursement_requests().status_code, 200)

    def test_pending_request(self):
        self.assertEqual(rs.pending_request().status_code, 200)

    def test_get_reimbursement_requests_by_emp_id(self):
        self.assertEqual(rs.get_reimbursement_requests_by_emp_id(100).status_code, 200)

    def test_pending_request_by_emp_id(self):
        self.assertEqual(rs.pending_request_by_emp_id(100).status_code, 200)

    def test_get_most_expenditure(self):
        self.assertEqual(rs.get_most_expenditure().status_code, 200)

    def test_get_most_approved(self):
        self.assertEqual(rs.get_most_approved().status_code, 200)

    def test_get_most_denied(self):
        self.assertEqual(rs.get_most_denied().status_code, 200)

    def test_get_mean_expenditure_per_employee(self):
        self.assertEqual(rs.get_mean_expenditure_per_employee().status_code, 200)
