import unittest
from unittest.mock import Mock
from src.services import employee_service as es
from src.dao import employee_dao as ed
from src.controller.flaskconfig import app


class TestEmployeeService(unittest.TestCase):
    def setUp(self):
        self.app_context = app.app_context()
        self.app_context.push()
        ed.get_user_pass_v2 = Mock(return_value=[('emp101', 'pass101', '0')])

    def tearDown(self):
        self.app_context.pop()

    def test_get_user_pass_v2(self):
        self.assertEqual(es.get_user_pass_v2('emp101', 'pass101', '0').status_code, 200)
