from unittest.mock import patch
import unittest
from src.dao import reimbursement_dao as rdao


class TestReimbursementDao(unittest.TestCase):
    def setUp(self):
        from pyodbc import connect
        db_url = 'localhost'
        db_username = 'postgres'
        db_password = 'kent123q'
        db_name = 'MockDBReimbursement'

        self.conn = connect(
            f"DRIVER={{PostgreSQL Unicode}};SERVER={db_url};PORT=5432;DATABASE={db_name};UID={db_username};PWD={db_password};Trusted_Connection=no")

    @patch('src.dao.reimbursement_dao.get_connection')
    def test_get_reimbursement_requests(self, mock_conn):
        mock_conn.return_value = self.conn
        mock_cursor = self.conn.cursor()
        mock_cursor.execute("select * from Reimbursement order by ticket_id")
        expected = mock_cursor.fetchall()
        actual = rdao.get_reimbursement_requests()
        self.assertEqual(expected, actual)

    @patch('src.dao.reimbursement_dao.get_connection')
    def test_pending_request(self, mock_conn):
        mock_conn.return_value = self.conn
        mock_cursor = self.conn.cursor()
        mock_cursor.execute("select * from Reimbursement where reimbursement_status = 'pending'")
        expected = mock_cursor.fetchall()
        actual = rdao.pending_request()
        self.assertEqual(expected, actual)

    @patch('src.dao.reimbursement_dao.get_connection')
    def test_get_reimbursement_requests_by_emp_id(self, mock_conn):
        mock_pass_emp_id = 101
        mock_fail_emp_id = -1
        mock_conn.return_value = self.conn
        mock_cursor = self.conn.cursor()
        mock_cursor.execute("select * from Reimbursement where employee_id = ? order by ticket_id", mock_pass_emp_id)
        expected_pass = mock_cursor.fetchall()
        mock_cursor.execute("select * from Reimbursement where employee_id = ? order by ticket_id", mock_fail_emp_id)
        expected_fail = mock_cursor.fetchall()
        actual = rdao.get_reimbursement_requests_by_emp_id(mock_pass_emp_id)
        self.assertEqual(expected_pass, actual)
        self.assertNotEqual(expected_fail, actual)

    @patch('src.dao.reimbursement_dao.get_connection')
    def test_pending_request_by_emp_id(self, mock_conn):
        mock_pass_emp_id = 101
        mock_fail_emp_id = -1
        mock_conn.return_value = self.conn
        mock_cursor = self.conn.cursor()
        mock_cursor.execute("select * from Reimbursement where reimbursement_status = 'pending' and employee_id = ?",
                            mock_pass_emp_id)
        expected_pass = mock_cursor.fetchall()
        mock_cursor.execute("select * from Reimbursement where reimbursement_status = 'pending' and employee_id = ?",
                            mock_fail_emp_id)
        expected_fail = mock_cursor.fetchall()
        actual = rdao.pending_request_by_emp_id(mock_pass_emp_id)
        self.assertEqual(expected_pass, actual)
        self.assertNotEqual(expected_fail, actual)

    @patch('src.dao.reimbursement_dao.get_connection')
    def test_get_most_expenditure(self, mock_conn):
        mock_conn.return_value = self.conn
        mock_cursor = self.conn.cursor()
        mock_cursor.execute("select employee_id, sum(reimbursement_amount) as most_expenditure "
                            "from reimbursement group by employee_id order by most_expenditure desc LIMIT 1")
        expected = mock_cursor.fetchall()
        actual = rdao.get_most_expenditure()
        self.assertEqual(expected, actual)

    @patch('src.dao.reimbursement_dao.get_connection')
    def test_get_most_approved(self, mock_conn):
        mock_conn.return_value = self.conn
        mock_cursor = self.conn.cursor()
        mock_cursor.execute("select employee_id, count(employee_id) as most_reimbursement_approved "
                            "from reimbursement where reimbursement_status = 'approved' "
                            "group by employee_id order by most_reimbursement_approved desc LIMIT 1")
        expected = mock_cursor.fetchall()
        actual = rdao.get_most_approved()
        self.assertEqual(expected, actual)

    @patch('src.dao.reimbursement_dao.get_connection')
    def test_get_most_denied(self, mock_conn):
        mock_conn.return_value = self.conn
        mock_cursor = self.conn.cursor()
        mock_cursor.execute("select employee_id, count(employee_id) as most_reimbursement_approved "
                            "from reimbursement where reimbursement_status = 'rejected' "
                            "group by employee_id order by most_reimbursement_approved desc LIMIT 1")
        expected = mock_cursor.fetchall()
        actual = rdao.get_most_denied()
        self.assertEqual(expected, actual)

    @patch('src.dao.reimbursement_dao.get_connection')
    def test_get_mean_expenditure_per_employee(self, mock_conn):
        mock_conn.return_value = self.conn
        mock_cursor = self.conn.cursor()
        mock_cursor.execute("select employee_id, avg(reimbursement_amount::numeric) as mean_expenditure "
                            "from reimbursement group by employee_id order by mean_expenditure desc")
        expected = mock_cursor.fetchall()
        actual = rdao.get_mean_expenditure_per_employee()
        self.assertEqual(expected, actual)


