from unittest.mock import patch
import unittest
from src.dao import employee_dao as edao


class TestEmployeeDao(unittest.TestCase):
    def setUp(self):
        from pyodbc import connect
        db_url = 'localhost'
        db_username = 'postgres'
        db_password = 'kent123q'
        db_name = 'MockDBReimbursement'

        self.conn = connect(
            f"DRIVER={{PostgreSQL Unicode}};SERVER={db_url};PORT=5432;DATABASE={db_name};UID={db_username};"
            f"PWD={db_password};Trusted_Connection=no")

    @patch('src.dao.employee_dao.get_connection')
    def test_get_employee_info(self, mock_conn):
        mock_conn.return_value = self.conn
        mock_cursor = self.conn.cursor()
        mock_cursor.execute("select * from Employee")
        expected = mock_cursor.fetchall()
        actual = edao.get_employee_info()
        self.assertEqual(expected, actual)

    @patch('src.dao.employee_dao.get_connection')
    def test_get_user_pass_emp(self, mock_conn):
        mock_username_emp_pass = 'emp101'
        mock_password_emp_pass = 'pass101'
        mock_is_manager_emp_pass = False
        mock_username_emp_fail = 'fail'
        mock_password_emp_fail = 'fail'
        mock_is_manager_emp_fail = False
        mock_conn.return_value = self.conn
        mock_cursor = self.conn.cursor()
        mock_cursor.execute("select employee_username, employee_password, is_manager from employee "
                            "where employee_username = ? and employee_password = ? and is_manager = ?",
                            mock_username_emp_pass, mock_password_emp_pass, mock_is_manager_emp_pass)
        expected_emp_pass = mock_cursor.fetchall()
        mock_cursor.execute("select employee_username, employee_password, is_manager from employee "
                            "where employee_username = ? and employee_password = ? and is_manager = ?",
                            mock_username_emp_fail, mock_password_emp_fail, mock_is_manager_emp_fail)
        expected_fail = mock_cursor.fetchall()
        actual_emp = edao.get_user_pass(mock_username_emp_pass, mock_password_emp_pass, mock_is_manager_emp_pass)
        self.assertEqual(expected_emp_pass, actual_emp)
        self.assertNotEqual(expected_fail, actual_emp)
