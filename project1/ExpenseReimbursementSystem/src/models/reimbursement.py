from json import JSONEncoder


class Reimbursement:
    def __init__(self, ticket_id, employee_id, reimbursement_type,
                 reimbursement_status, reimbursement_amount,
                 reimbursement_date, reimbursement_purpose):
        self.ticket_id = ticket_id
        self.employee_id = employee_id
        self.reimbursement_type = reimbursement_type
        self.reimbursement_status = reimbursement_status
        self.reimbursement_amount = reimbursement_amount
        self.reimbursement_date = str(reimbursement_date)
        self.reimbursement_purpose = reimbursement_purpose


class ReimbursementEncoder(JSONEncoder):
    # In order to define how your type should be serialized, you should override the "default"
    # method:
    def default(self, reimbursement):
        if isinstance(reimbursement, Reimbursement):
            return reimbursement.__dict__
        else:
            # If something should go wrong, just fall back on the default (parent) implementation
            # of this method
            return super().default(reimbursement)


class Statistics:
    def __init__(self, employee_id, total):
        self.employee_id = employee_id
        self.total = total


class MeanExpenditure:
    def __init__(self, emp_id, mean_exp):
        self.employee_id = emp_id
        self.mean_exp = round(float(mean_exp), 2)
