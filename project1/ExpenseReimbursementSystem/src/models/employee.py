from json import JSONEncoder


class Employee:
    def __init__(self, employee_id, employee_firstname,
                 employee_lastname, employee_email,
                 employee_username, employee_password, is_manager):
        self.employee_id = employee_id
        self.employee_firstname = employee_firstname
        self.employee_lastname = employee_lastname
        self.employee_email = employee_email
        self.employee_username = employee_username
        self.employee_password = employee_password
        self.is_manager = is_manager


class EmployeeEncoder(JSONEncoder):
    # In order to define how your type should be serialized, you should override the "default"
    # method:
    def default(self, employee):
        if isinstance(employee, Employee):
            return employee.__dict__
        else:
            # If something should go wrong, just fall back on the default (parent) implementation
            # of this method
            return super().default(self, employee)


class Login:
    def __init__(self, employee_username, employee_password, is_manager):
        self.employee_username = employee_username
        self.employee_password = employee_password
        self.is_manager = is_manager
