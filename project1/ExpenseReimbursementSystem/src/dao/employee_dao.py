# This module is for handling getting data access from the DB
from src.utils.dbconfig import get_connection
from src.logging.logging_setup import employeeLog


def get_employee_info():
    employeeLog.info('View all Employees information')
    try:
        db_connection = get_connection()
        db_cursor = db_connection.cursor()
        db_cursor.execute("select * from Employee")
        query_rows = db_cursor.fetchall()

    finally:
        db_connection.close()

    return query_rows


def get_user_pass(username, password, is_manager):
    employeeLog.info('Getting the right credentials for Employee or Manager')
    try:
        db_connection = get_connection()
        db_cursor = db_connection.cursor()
        db_cursor.execute("select employee_username, employee_password, is_manager from employee "
                          "where employee_username = ? and employee_password = ? and is_manager = ?",
                          username, password, is_manager)
        query_rows = db_cursor.fetchall()

    finally:
        db_connection.close()

    return query_rows

