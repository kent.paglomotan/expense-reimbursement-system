# This module is for handling getting data access from the DB
from src.utils.dbconfig import get_connection
from src.logging.logging_setup import reimbursementLog


def get_reimbursement_requests():
    reimbursementLog.info('View all reimbursement requests from the employee')
    # Start by getting a connection to a DB
    try:
        db_connection = get_connection()
        # Then we need a cursor object to manage the context of operation
        db_cursor = db_connection.cursor()
        # Then we need to execute an SQL query/statements in the DB
        db_cursor.execute("select * from Reimbursement order by ticket_id")
        # After executing the query just fetch all, returns a collection of sequences (tuples)
        query_rows = db_cursor.fetchall()

    finally:
        # No matter what happens always close the connection to the DB
        db_connection.close()

    return query_rows


def pending_request():
    reimbursementLog.info('Getting only the pending request of all the employees')
    try:
        db_connection = get_connection()
        db_cursor = db_connection.cursor()
        db_cursor.execute("select * from Reimbursement where reimbursement_status = 'pending'")
        query_rows = db_cursor.fetchall()

    finally:
        db_connection.close()

    return query_rows


def get_reimbursement_requests_by_emp_id(employee_id):
    reimbursementLog.info('Getting only the reimbursement for an employee')
    reimbursementLog.info('The employee ID' + str(employee_id) + 'is requested to all of their reimbursement')
    # Start by getting a connection to a DB
    try:
        db_connection = get_connection()
        # Then we need a cursor object to manage the context of operation
        db_cursor = db_connection.cursor()
        # Then we need to execute an SQL query/statements in the DB
        db_cursor.execute("select * from Reimbursement where employee_id = ? order by ticket_id", employee_id)
        # After executing the query just fetch all, returns a collection of sequences (tuples)
        query_rows = db_cursor.fetchall()

    finally:
        # No matter what happens always close the connection to the DB
        db_connection.close()

    return query_rows


def pending_request_by_emp_id(employee_id):
    reimbursementLog.info('Getting only the pending request for an employee')
    reimbursementLog.info('The employee ID' + str(employee_id) + 'is requested to all of pending reimbursement')
    try:
        db_connection = get_connection()
        db_cursor = db_connection.cursor()
        db_cursor.execute("select * from Reimbursement where reimbursement_status = 'pending' and employee_id = ?",
                          employee_id)
        query_rows = db_cursor.fetchall()

    finally:
        db_connection.close()

    return query_rows


def submit_request(employee_id, reimbursement_type, amount, purpose):
    reimbursementLog.info('Submitting reimbursement request with the Employee ID ')
    reimbursementLog.info('The employee ID: ' + str(employee_id) + ' The Type of Reimbursement: '
                          + str(reimbursement_type) + ' The Amount: ' + str(amount)
                          + ' The Purpose of reimbursement ' + str(purpose))
    try:
        db_connection = get_connection()
        db_cursor = db_connection.cursor()
        db_cursor.execute("insert into Reimbursement values "
                          "(default, ?, ?, default, ?, default, ?)",
                          employee_id, reimbursement_type, amount, purpose)
        db_connection.commit()

    finally:
        db_connection.close()


def get_most_expenditure():
    reimbursementLog.info('Getting the most expenditure, meaning an employee who spends the most')
    try:
        db_connection = get_connection()
        db_cursor = db_connection.cursor()
        db_cursor.execute("select employee_id, sum(reimbursement_amount) as most_expenditure "
                          "from reimbursement group by employee_id order by most_expenditure desc LIMIT 1")
        query_rows = db_cursor.fetchall()

    finally:
        db_connection.close()

    return query_rows


def get_most_approved():
    reimbursementLog.info('Getting the most approved reimbursement from the one employee')
    try:
        db_connection = get_connection()
        db_cursor = db_connection.cursor()
        db_cursor.execute("select employee_id, count(employee_id) as most_reimbursement_approved "
                          "from reimbursement where reimbursement_status = 'approved' "
                          "group by employee_id order by most_reimbursement_approved desc LIMIT 1")
        query_rows = db_cursor.fetchall()

    finally:
        db_connection.close()

    return query_rows


def get_most_denied():
    reimbursementLog.info('Getting the most denied reimbursement from the one employee')
    try:
        db_connection = get_connection()
        db_cursor = db_connection.cursor()
        db_cursor.execute("select employee_id, count(employee_id) as most_reimbursement_approved "
                          "from reimbursement where reimbursement_status = 'rejected' "
                          "group by employee_id order by most_reimbursement_approved desc LIMIT 1")
        query_rows = db_cursor.fetchall()

    finally:
        db_connection.close()

    return query_rows


def get_mean_expenditure_per_employee():
    reimbursementLog.info('Getting the most denied reimbursement from the one employee')
    try:
        db_connection = get_connection()
        db_cursor = db_connection.cursor()
        db_cursor.execute("select employee_id, avg(reimbursement_amount::numeric) as mean_expenditure "
                          "from reimbursement group by employee_id order by mean_expenditure desc")
        query_rows = db_cursor.fetchall()

    finally:
        db_connection.close()

    return query_rows


def accept_request(ticket_id):
    reimbursementLog.info('Accepting request from the employee ID ')
    reimbursementLog.info('Accepting this ticket ID: ' + str(ticket_id))
    try:
        db_connection = get_connection()
        db_cursor = db_connection.cursor()
        db_cursor.execute("update reimbursement set reimbursement_status = 'approved' where ticket_id = ?",
                          ticket_id)
        db_connection.commit()

    finally:
        db_connection.close()


def deny_request(ticket_id):
    reimbursementLog.info('Accepting request from the employee ID ')
    reimbursementLog.info('Denying this ticket ID: ' + str(ticket_id))
    try:
        db_connection = get_connection()
        db_cursor = db_connection.cursor()
        db_cursor.execute("update reimbursement set reimbursement_status = 'rejected' where ticket_id = ?",
                          ticket_id)
        db_connection.commit()

    finally:
        db_connection.close()
